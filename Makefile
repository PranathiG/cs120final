# Lines starting with # are comments                                              

# Some variable definitions to save typing later on 
CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g


# Default (first) target
chess: ChessMain.o Chess.o Board.o Piece.o Cases.o
	$(CXX) $(CXXFLAGS) ChessMain.o Chess.o Board.o Piece.o Cases.o -o chess

# Compiles unittest
unittest: unittest.o Chess.o Board.o Piece.o Cases.o
	$(CXX) $(CXXFLAGS) unittest.o Chess.o Board.o Piece.o Cases.o -o unittest

# Compiles into object file
unittest.o: unittest.cpp
	$(CXX) $(CXXFLAGS) -c unittest.cpp

Board.o: Board.cpp
	$(CXX) $(CXXFLAGS) -c Board.cpp

Chess.o: Chess.cpp
	$(CXX) $(CXXFLAGS) -c Chess.cpp

ChessMain.o: ChessMain.cpp
	$(CXX) $(CXXFLAGS) -c ChessMain.cpp

Piece.o: Piece.cpp
	$(CXX) $(CXXFLAGS) -c Piece.cpp

Cases.o: Cases.cpp
	$(CXX) $(CXXFLAGS) -c Cases.cpp

# 'make clean' will remove intermediate & executable files                        
clean:
	rm -f *.o chess unittest
