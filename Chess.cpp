#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include <math.h> //for exponentiation with pow in printBoard 
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;

int Piece::validMove(Position start, Position end,const Board& board) const {
    // This particular method may include generic logic to check
    // for a valid move.

    //  if (this->owner() != board.playerTurn()) {
    //return MOVE_ERROR_NO_PIECE; //invalid move because start position is of wrong piece id
    // }
    //test if player's piece is attacking its own piece
    Piece* testPointer3;
    testPointer3 = board.getPiece(start);

    Piece* testPointer2;
    testPointer2 = board.getPiece(end);
    if (testPointer2 == NULL) {
        return SUCCESS;
    } else if (testPointer2->owner() == testPointer3->owner()) { //this->owner()) {
        return  MOVE_ERROR_ILLEGAL; //player tried to take it's own piece
    } else { //player has captured the other player's piece
      return MOVE_CAPTURE;
    }
    return SUCCESS;
}

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    // Possibly implement chess-specific move logic here
    // We call Board's makeMove to handle any general move logic
    // Feel free to use this or change it as you see fit

    if (m_pieces[index(start)] == NULL) {
        Prompts::noPiece();
        return MOVE_ERROR_NO_PIECE;
    }
    int retCode2;
    int retCode = Board::makeMove(start, end);
    if (retCode >= 0) { //if it is valid (so valid = 1) then moves piece

        Piece* testPointer;
        testPointer = getPiece(start);
	
	if (testPointer->owner() != this->playerTurn()) {
            retCode = MOVE_ERROR_NO_PIECE; //invalid move because start position is of wrong piece id
            this->checkPrompts(retCode, testPointer->owner());
            return retCode;
        }

        //this function goes to find the id of the Piece and go to the specific validMove
        retCode = testPointer->validMove(start, end, *this);
        if (retCode >=0) {
            retCode2 = this->specialCircumstances(start, end); //checks to see if stalemate, checkmate, etc.
	    if (retCode2 != 678) {
	      retCode = retCode2;
	    } 
            if (retCode >= 0) {
                if (this->playerTurn() == 0) {
                    whiteWasChecked = 0;
                } else {
                    blackWasChecked = 0;
                }
		//make move
		Piece* test;
		test = getPiece(end);
		delete(test);
                m_pieces[index(end)] = testPointer;
                m_pieces[index(start)] = nullptr;
		
		retCode2 = this->otherPlayerInCheck(start, end); //check if next player in check
	   
		if (retCode2 == MOVE_CHECK) { //if it is in check
		  retCode2 = this->anyValidMoves();
		} else { //if it is not in check
		  retCode2 = this->anyValidMoves();
		  if (retCode2 == MOVE_CHECKMATE) {
		    retCode2 = MOVE_STALEMATE;
		  }
		}
	    }
        }
	if (retCode2 != 123) {
	  if (this->checkPrompts(retCode2, testPointer->owner()) == GAME_OVER) {
            return GAME_OVER;
	  }
        } else if (this->checkPrompts(retCode, testPointer->owner()) == GAME_OVER) {
            return GAME_OVER;
	}
    }
    return retCode; //>= if valid, < 0 if not valid
    //MUST ADD TO VALIDMOVE METHOD IN GAME.H!!!
}

int ChessGame::checkPrompts(int x, Player player) {
    switch(x) {
    case MOVE_CHECKMATE:
        Prompts::checkMate(player);
        Prompts::win(this->playerTurn(), this->turn());
        Prompts::gameOver();
        return GAME_OVER;
        break; //BUT HOW DO I FREE MEMORY AND BREAK FOR GOOD?
    case MOVE_STALEMATE:
        Prompts::staleMate();
        Prompts::gameOver();
        return GAME_OVER;
        break; //BUT HOW DO I FREE MEMORY AND BREAK FOR GOOD?
    case MOVE_CHECK:
        Prompts::check(player);
        break;
    case MOVE_CAPTURE:
        Prompts::capture(player);
        break;
    case MOVE_ERROR_OUT_OF_BOUNDS:
        Prompts::outOfBounds();
        break;
    case MOVE_ERROR_NO_PIECE:
        Prompts::noPiece();
        break;
    case MOVE_ERROR_ILLEGAL:
        Prompts::illegalMove();
        break;
    case MOVE_ERROR_BLOCKED:
        Prompts::blocked();
        break;
    case MOVE_ERROR_MUST_HANDLE_CHECK:
        Prompts::mustHandleCheck();
        break;
    case MOVE_ERROR_CANT_EXPOSE_CHECK:
        Prompts::cantExposeCheck();
        break;
    case MOVE_ERROR_CANT_CASTLE:
        Prompts::cantCastle();
        break;
    }
    return 0;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

// Save chess board with data into a file
void ChessGame:: saveBoard() {
    string user;
    Prompts::saveGame();
    getline(cin,user);

    ofstream fp;
    fp.open(user);
    int max = user.length();
    if ( max < 5 || !fp.is_open() || (user.substr(max-4, max-1) != ".txt")) {
        //if saving fails for some reason
        Prompts::saveFailure();
        return;
    }

    fp << "chess" << endl << (this->turn() - 1) << endl;
    Position currFilePrint;
    Piece* testPointer;
    const int asciiLetter = 97;
    for (int r = 7; r >= 0; r--) {
        for (int c = 0; c < 8; c++) {
            currFilePrint.x = c; //must convert to alphabetical letters
            currFilePrint.y = r; //must covert so it goes from 1 to 8
            char letter = (char) (c + asciiLetter);
            testPointer = getPiece(currFilePrint);
            if (testPointer != NULL) {
                fp << (*testPointer).owner() << " " << letter << (r + 1) << " " << (*testPointer).id() << endl;
            }
        }

    }
    fp.close();
}

// Load chess board with data from file
int ChessGame::loadBoard() {
    string fileName;
    Prompts::loadGame();
    getline(cin,fileName);
    string txtLine;
    //load in file with saved game
    ifstream fp(fileName);
    if (!fp.is_open()) {
        Prompts::loadFailure();
        return LOAD_FAILURE;
    }

    int counter = 0; //keeps track of what line in file we are on
    //if files does open
    while (getline(fp, txtLine)) {
        counter++;
        if (counter == 2) {
            this->m_turn = stoi(txtLine);
        }

        if (counter > 2 ) {
            int pieceInt = stoi(txtLine.substr(5,1));
            string letter = txtLine.substr(2,1);
            int x = tolower(letter[0]) - 'a';
            int y = stoi(txtLine.substr(3,1));

            if (txtLine.substr(0,1) == "0") {
                initPiece(pieceInt, WHITE, Position(x,y - 1));
            } else {
                initPiece(pieceInt, BLACK, Position(x,y - 1));
            }

        }
    }
    fp.close();
    this->iterateTurn();
    return 0;
}

void Board::invertColors(bool* inverted) {
    if (*inverted) {
        Terminal::colorBg(Terminal::WHITE);
    } else {
        Terminal::colorBg(Terminal::BLACK);
    }
    *inverted ^= 1;

}
void Board::printBoard() {
    // Start with white square in top left as foretold in the prophecies
    Position currPrint;
    Piece* testPointer;
    bool inverted = false;
    int owner;
    //char *test = u8"\u2654";
    // Print the top row with the column labels
    Terminal::colorAll(false, Terminal::BLACK, Terminal::WHITE);
    cout << "  ";
    for (char columnLabels = 'A'; columnLabels < 'I'; columnLabels++) {
        cout << "  " << columnLabels << "  ";
    }
    Terminal::set_default();
    cout << endl;
    // Print out the board
    // Start with white square in top left as foretold in the prophecies
    Terminal::colorBg(Terminal::WHITE);
    for (int boardRows = 7; boardRows>=0; boardRows--) {
        for (int squareRows = 2; squareRows>=0; squareRows--) {
            // print out the row labels
            Terminal::colorAll(false, Terminal::BLACK, Terminal::WHITE);
            if (squareRows == 1) {
                cout << boardRows + 1 << " ";
            } else {
                cout << "  ";
            }
            if (inverted) {
                Terminal::colorBg(Terminal::BLACK);
            } else {
                Terminal::colorBg(Terminal::WHITE);
            }
            for (int boardColumns = 0; boardColumns<8; boardColumns++) {
                for (int squareColumns=0; squareColumns<5; squareColumns++) {
                    if (squareRows == 1 && squareColumns == 2) {
                        currPrint.x = boardColumns;
                        currPrint.y = boardRows;
                        testPointer = getPiece(currPrint);
                        if (testPointer == NULL) {
                            cout << " ";
                        } else {
                            // Print piece character (Q for queen, R for rook, etc.)
                            // This is a magic polynomial function that gets the piece character from its enumeration
                            char x = testPointer->id();
                            owner = testPointer->owner();
                            if (owner == 0) {
                                Terminal::colorFg(true, Terminal::WHITE);
                            } else {
                                Terminal::colorFg(true, Terminal::BLACK);
                            }
                            cout << (char)(-pow(x, 5) + 11.542*pow(x, 4) - 44.583*pow(x, 3) + 64.958*pow(x,2) - 28.917*x + 80);
                        }
                    } else {
                        cout << " ";
                    }
                }
                // invert after each square
                Board::invertColors(&inverted);
            }
            // default-colored new line after each square row
            Terminal::set_default();
            cout << endl;
            // return to current color scheme - first "invert" from default to wrong colors, then invert again to correct ones
            Board::invertColors(&inverted);
            Board::invertColors(&inverted);
        }
        // invert before each board row
        Board::invertColors(&inverted);
    }
    Terminal::set_default();
}

//checks to see if we should print board or not
int ChessGame::checkBoard(int boardON) {
    if (boardON == 0) {
        this->printBoard();
        return boardON + 1;
    } else if (boardON == 1) {
        return boardON - 1;
    }
    return boardON;
}

//main gameplay loop
void ChessGame::run() { //after setting up board or loading in new game, this keeps track of player turns and if black or white

    Prompts::playerPrompt(this->playerTurn(), this->turn());
    string user;
    int boardON = 0;
    int doNotMove = 0;
    getline(cin, user);
    int code;

    while (user != "q" && user != "Q") {
        doNotMove = 0; //restarts this every turn

        // Converts input to lowercase for case-insensitivity
        for (int i=0; user[i]; i++) {
            user[i] = tolower(user[i]);
        }

        if (user == "board") {
            boardON = this->checkBoard(boardON);
            doNotMove = 1;
        }

        if (user == "save") {
            this->saveBoard(); //goes to the saveBoard function to save the data
            doNotMove = 1;
        }

        if (user == "forfeit") {
            this->iterateTurn();
            Prompts::win(this->playerTurn(), this->turn());
            Prompts::gameOver();
            break; 
        }

        if (!doNotMove && this->runOption(user) == 1) { //takes in the input and breaks it down to figure out movement positions
            Position start, end;
            start.x = tolower(user[0]) - 'a'; //case insenstive
            start.y = user[1] -'0' - 1;
            end.x = tolower(user[3]) - 'a'; //case insensitive
            end.y = user[4] -'0' - 1;
            code = this->makeMove(start, end);
            if (code>= 0) {
                if (boardON) {
                    this->printBoard();
                }
                this->iterateTurn();
            }
	    if (code == GAME_OVER) {
                break;
            }
        }
        //Prompt line must be out of the if (!doNotMove) statement
        Prompts::playerPrompt(this->playerTurn(), this->turn());
        getline(cin, user);
    }
    // return 0;
}

//before parsing, must check if the input is of correct format
//checks if the input is of the form "x4 z5" so we can parce it
int ChessGame::runOption(string &user) {
    if (!isalpha(user[0]) || !isalpha(user[3]) || !isspace(user[2])) {
        Prompts::parseError();
        return MOVE_ERROR_OUT_OF_BOUNDS;
    } else if (!isdigit(user[1]) || !isdigit(user[4])|| (user.length() != 5)) {
        Prompts::parseError();
        return MOVE_ERROR_OUT_OF_BOUNDS;
    } else if ( user[1] == '0' || user[4] == '0') {
        Prompts::outOfBounds();
        return MOVE_ERROR_OUT_OF_BOUNDS; //checks if any of the numbers are 0, otherwise it'll fail when parsing it
    } else { //input is in the general form
        return SUCCESS; //returns 0 if valid
    }
}

void Board::iterateTurn() { //increases number of turns during every other loop
    m_turn++;
}

int ChessGame::menuOption(string &input1) {
    if (input1 == "1" ) {
        this->setupBoard();
        return SUCCESS;
    } else if (input1 == "2") {
        int loadFailed = this->loadBoard(); //checks what the return int
        if (loadFailed == -10) { //if board failed to load properly quit the program
            return LOAD_FAILURE;
        }
        return SUCCESS;
    }
    return 0;
}



