#include "Game.h"
#include "Chess.h"
#include <math.h> // for pow in pawn validMove   
#include <algorithm> // for min/max in rook, etc validMove
using namespace std;

  int Pawn::validMove(Position start, Position end,
                  const Board& board) const {
        int moveSign;
        int startx = start.x;
        int starty = start.y;
        int endx = end.x;
        int endy = end.y;
        Piece* testPointer = board.getPiece(end);
	Position testPos;
        // moveSign accounts for the opposite pawn movement directions of the two players
        if (this->owner() == 0) {
            moveSign = 1;
        } else {
            moveSign = -1;
        }

	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
	  return retCode; // invalid - general reasons (taking own piece, perhaps)                          
        }
        if ((endy-starty)*moveSign < 1) {
            return MOVE_ERROR_ILLEGAL; // invalid - not moving forward
        }
        if (testPointer != NULL && ((endy-starty)*moveSign != 1 || abs(endx-startx) != 1)) {
            return MOVE_ERROR_ILLEGAL; // invalid - attacking to an invalid square
        }
        if (testPointer == NULL && endx != startx) {
            return MOVE_ERROR_ILLEGAL; //invalid - moving sideways when not attacking
        }
        if (starty == 5*this->owner() + 1 && (endy-starty)*moveSign > 2) {
            return MOVE_ERROR_ILLEGAL; // invalid - moving more than 2 squares forward from home row
        }
	testPos.y = starty + moveSign;
	testPos.x = startx;
	testPointer = board.getPiece(testPos);
	if ((endy-starty)*moveSign == 2 && testPointer != NULL) {
            return MOVE_ERROR_BLOCKED; // invalid - piece in the way of 2-square move
        }
        if (starty != 5*this->owner() + 1 && (endy-starty)*moveSign > 1) {
            return MOVE_ERROR_ILLEGAL; // invalid - moving more than 1 square from non-home row
        }
        return retCode;
    }

  int Rook::validMove(Position start, Position end,
                  const Board& board) const {
        Position testPos;
        int startx = start.x;
        int starty = start.y;
        int endx = end.x;
        int endy = end.y;

	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
          return retCode; // invalid - general reasons (taking own piece, perhaps)                         
        }
         if (endx - startx != 0 && endy - starty != 0) {
            return MOVE_ERROR_ILLEGAL; // invalid - not in a straight line
        }
        // if in same horizontal line
        if (endy == starty) {
            testPos.y = starty;
            for (int i = min(startx, endx)+ 1; i<max(startx, endx); i++) {
                testPos.x = i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        // if in same vertical line
        if (endx == startx) {
            testPos.x = startx;
            for (int i = min(starty, endy) + 1; i<max(starty, endy); i++) {
                testPos.y = i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        return retCode;
    }

int Knight::validMove(Position start, Position end,
                  const Board& board) const {

	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
          return retCode; // invalid - general reasons (taking own piece, perhaps)                              
        }
        if (pow(((int)end.x - (int)start.x), 2) + pow(((int)end.y - (int)start.y), 2) !=5) {
            return MOVE_ERROR_ILLEGAL; // invalid - not an 'L' move
        }
        return retCode;
    }


int Bishop::validMove(Position start, Position end,
                  const Board& board) const {
        Position testPos;
        int startx = start.x;
        int starty = start.y;
        int endx = end.x;
        int endy = end.y;

	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
          return retCode; // invalid - general reasons (taking own piece, perhaps)                         
        }
        if (endx - startx != endy - starty && endx - startx != starty - endy) {
            return MOVE_ERROR_ILLEGAL; // invalid - not in a diagonal line
        }
        // if in same 45 degree line
        if (endx - startx == endy - starty) {
            for (int i = 1; i<abs(endx-startx); i++) {
                testPos.x = min(startx, endx) + i;
                testPos.y = min(starty, endy) + i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        // if in same 135 degree line
        if (endx - startx == starty - endy) {
            for (int i = 1; i<abs(endx-startx); i++) {
                testPos.x = min(startx, endx) + i;
                testPos.y = max(starty, endy) - i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        return retCode;
    }


int Queen::validMove(Position start, Position end,
                  const Board& board) const {
        Position testPos;
	int startx = start.x;
        int starty = start.y;
        int endx = end.x;
        int endy = end.y;
	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
          return retCode; // invalid - general reasons (taking own piece, perhaps)               
        }
        if (endx - startx != 0 && endy - starty != 0 && endx - startx != endy - starty && endx - startx != starty - endy) {
            return MOVE_ERROR_ILLEGAL; // invalid - not in a straight or diagonal line
        }
        // if in same horizontal line
        if (endy == starty) {
            testPos.y = starty;
            for (int i = min(startx, endx)+ 1; i<max(startx, endx); i++) {
                testPos.x = i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        // if in same vertical line
        if (endx == startx) {
            testPos.x = startx;
            for (int i = min(starty, endy) + 1; i<max(starty, endy); i++) {
                testPos.y = i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        // if in same 45 degree line
        if (endx - startx == endy - starty) {
            for (int i = 1; i<abs(endx-startx); i++) {
                testPos.x = min(startx, endx) + i;
                testPos.y = min(starty, endy) + i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        // if in same 135 degree line
        if (endx - startx == starty - endy) {
            for (int i = 1; i<abs(endx-startx); i++) {
                testPos.x = min(startx, endx) + i;
                testPos.y = max(starty, endy) - i;
                if (board.getPiece(testPos) != NULL) {
                    return MOVE_ERROR_BLOCKED; // invalid - pieces in the way
                }
            }
        }
        return retCode;
    }

int King::validMove(Position start, Position end,
                  const Board& board) const {

      	int retCode = Piece::validMove(start, end, board);
        if (retCode < 0) {
          return retCode; // invalid - general reasons (taking own piece, perhaps)                    
        }
	if (pow(((int)end.x - (int)start.x), 2) + pow(((int)end.y - (int)start.y), 2) > 2) {
            return MOVE_ERROR_ILLEGAL; // invalid - more than 1 tile away
        }
        return retCode;
    }
