/* File: unittest.cpp                                                       
   Final Project, 600.120 Spring 2017                                           
*/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <string.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;
#include <assert.h>

//random output appears on the screen but can be ignored!

//test King moves                                                                                                        
void test_validMoveKing() {
  ChessGame chess;
  chess.setupBoard();
    Position start, end;
  Piece* test;
  
  //pawn e2 to e4 valid                                                                                                   
  start.x =  3; //case insenstive                                                                                         
  start.y = 1;
  end.x = 3; //case insensitive                                                                                           
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);
  
  //king e1 to e2                                                                                                        
  start.x = 4; //case insenstive                                                                                          
  start.y = 0;
  end.x = 4; //case insensitive                                                                                           
  end.y = 1;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

  //king e1 to f2                                                                                                        
  start.x = 4; //case insenstive                                                                                          
  start.y = 0;
  end.x = 5; //case insensitive                                                                                           
  end.y = 1;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

}

//test Queen moves                                                                                                           
void test_validMoveQueen() {
  ChessGame chess;
  chess.setupBoard();
  Position start, end;
  Piece* test;

  //pawn d2 to d4 valid                                                                                                   
  start.x =  3; //case insenstive
  start.y = 1;
  end.x = 3; //case insensitive
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //queen d1 to d2 
  start.x = 3; //case insenstive                                                                                          
  start.y = 0;
  end.x = 3; //case insensitive                                                                                           
  end.y = 1;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);
  
  //queen d1 to e2                                                                                                      
  start.x = 3; //case insenstive                                                                                          
  start.y = 0;
  end.x = 3; //case insensitive                                                                                           
  end.y = 1;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);
}

//test Bishop moves                                                                                                           
void test_validMoveBishop() {
 ChessGame chess;
  chess.setupBoard();
  Position start, end;
  Piece* test;
  //move a pawn out of the way
  //pawn d2 to d4 valid                                                                                                   
  start.x =  3; //case insenstive
  start.y = 1;
  end.x = 3; //case insensitive
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //bishop c1 to h6 valid                                                                                              
  start.x = 2; //case insenstive                                                                                         
  start.y = 0;
  end.x = 7; //case insensitive                                                                                           
  end.y = 5;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);

  //bishop f1 to d3 valid                                                                                                 
  start.x = 6; //case insenstive                                                                                          
  start.y = 0;
  end.x = 3; //case insensitive                                                                                           
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);
  
}

//test Knight moves
void test_validMoveKnight() {
  ChessGame chess;
  chess.setupBoard();
  Position start, end;
  Piece* test;

  //knight b1 to a3 valid                                                                                                
  start.x =  1; //case insenstive
  start.y = 0;
  end.x = 0; //case insensitive
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);

  //knight g1 to h3 valid
  start.x =  6; //case insenstive
  start.y = 0;
  end.x = 7; //case insensitive
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);

}

//test Rook moves
void test_validMoveRook() {
  ChessGame chess;
  chess.setupBoard();
  Position start, end;
  Piece* test;
  //move a pawn out of the way
  //pawn a2 to a4 valid                                                              
  start.x =  0; //case insenstive                                                      
  start.y = 1;
  end.x = 0; //case insensitive                                                        
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);
  
  //rook a1 to a3 valid
  start.x =  0; //case insenstive	     
  start.y = 0;
  end.x = 0; //case insensitive
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);

  //rook h1 to a3 invalid                                                                
  start.x =  7; //case insenstive                                                      
  start.y = 0;
  end.x = 0; //case insensitive                                                        
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

  
}


void test_validMovePawn() {
  ChessGame chess;
  chess.setupBoard();
  Position start, end;
  Piece* test;

  //pawn a2 to a4 valid                                                                                  
  start.x =  0; //case insenstive                                                                        
  start.y = 1;
  end.x = 0; //case insensitive                                                                          
  end.y = 3;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == 1);

  //pawn b2 to b5 invalid
  start.x =  1; //case insenstive
  start.y = 1;
  end.x = 1; //case insensitive
  end.y = 4;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

  //pawn c1 to c2 invalid
  start.x =  2; //case insenstive
  start.y = 0;
  end.x = 0; //case insensitive
  end.y = 1;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

  //pawn d2 to c3 valid
  start.x =  3; //case insenstive
  start.y = 1;
  end.x = 2; //case insensitive
  end.y = 2;
  test = chess.getPiece(start);
  assert(test->validMove(start, end, chess) == -1);

}

//tests to see if makeMoves correctly analyzes input and gives proper result back
void test_makeMove() {
  ChessGame chess;
  chess.setupBoard();
  Position start, end;

  //pawn a2 to a4 valid
  start.x =  0; //case insenstive                   
  start.y = 1;
  end.x = 0; //case insensitive                    
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //pawn to h7 h5 invalid because wrong player's piece
  start.x =  4; //case insenstive                                                             
  start.y = 6;
  end.x = 4; //case insensitive                                                               
  end.y = 4;
  assert(chess.makeMove(start, end) == MOVE_ERROR_NO_PIECE);

  //empty space move
  start.x =  1; //case insenstive                                                           
  start.y = 4;
  end.x = 1; //case insensitive                                                              
  end.y = 5;
  assert(chess.makeMove(start, end) == MOVE_ERROR_NO_PIECE);

  //rook a1 to a5 invalid because blocked by another piece
  start.x =  0; //case insenstive                                                             
  start.y = 0;
  end.x = 0; //case insensitive                                                               
  end.y = 4;
  assert(chess.makeMove(start, end) == MOVE_ERROR_BLOCKED);
	 
  //rook a1 to a3 is valid
  start.x =  0; //case insenstive                                                             
  start.y = 0;
  end.x = 0; //case insensitive                                                               
  end.y = 2;
  assert(chess.makeMove(start, end) == 123);

  //pawn a4 to b5 invalid because can only attack on diagonal
  start.x =  3; //case insenstive                                                             
  start.y = 6;
  end.x = 3; //case insensitive                                                               
  end.y = 4;
  assert(chess.makeMove(start, end) == MOVE_ERROR_NO_PIECE);

  //knight g1 to g3 invalid           
  start.x =  6; //case insenstive                                                             
  start.y = 0;
  end.x = 6; //case insensitive                                                               
  end.y = 2;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);
  
  //knight g1 to f3 valid
  start.x =  6; //case insenstive                                                             
  start.y = 0;
  end.x = 5; //case insensitive                                                               
  end.y = 2;
  assert(chess.makeMove(start, end) == 123);

  //knight f3 to d4 valid
  start.x =  5; //case insenstive                                                             
  start.y = 2;
  end.x = 3; //case insensitive                                                               
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //pawn from d2 to d4 invalid because knight right there
  start.x =  3; //case insenstive                                                             
  start.y = 1;
  end.x = 3; //case insensitive                                                               
  end.y = 3;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);

  //pawn from d2 to d3 valid
  start.x =  3; //case insenstive                                                             
  start.y = 1;
  end.x = 3; //case insensitive                                                               
  end.y = 2;
  assert(chess.makeMove(start, end) == 123);

  //bishop from c1 to b2 invalid because another piece there
  start.x =  2; //case insenstive                                                             
  start.y = 0;
  end.x = 1; //case insensitive                                                               
  end.y = 1;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);

  //bishop from c1 to f4
  start.x =  2; //case insenstive                                                             
  start.y = 0;
  end.x = 5; //case insensitive                                                               
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //queen from d1 to e4 invalid
  start.x = 3; //case insenstive                                                             
  start.y = 0;
  end.x = 4; //case insensitive                                                               
  end.y = 3;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);

  //queen from d1 to a4 invalid                                                                          
  start.x = 3; //case insenstive                                                                         
  start.y = 0;
  end.x = 0; //case insensitive                                                                          
  end.y = 3;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);

  //queen from d1 to d5 invalid                                                                        
  start.x = 3; //case insenstive                                                                         
  start.y = 0;
  end.x = 3; //case insensitive                                                                          
  end.y = 4;
  assert(chess.makeMove(start, end) == MOVE_ERROR_BLOCKED);

  //queen from d1 to h5 valid                                                                          
  start.x = 3; //case insenstive                                                                         
  start.y = 0;
  end.x = 7; //case insensitive                                                                          
  end.y = 4;
  assert(chess.makeMove(start, end) == MOVE_ERROR_BLOCKED);

  //now change the player so we can test valid classes in the other direction
  //also check attack plays
  chess.iterateTurn(); //so now can test black pieces and attacks!
  
  //pawn b7 to b5 valid                                                                                  
  start.x =  1; //case insenstive                                                                        
  start.y = 6;
  end.x = 1; //case insensitive                                                                          
  end.y = 4;
  assert(chess.makeMove(start, end) == 123);

  //pawn b5 to c4 invalid                                                                         
  start.x =  1; //case insenstive                                                                        
  start.y = 4;
  end.x = 2; //case insensitive                                                                          
  end.y = 3;
  assert(chess.makeMove(start, end) == MOVE_ERROR_ILLEGAL);

  //pawn b2 to b3 invalid                                                                               
  start.x =  1; //case insenstive                                                                        
  start.y = 1;
  end.x = 1; //case insensitive                                                                          
  end.y = 2;
  assert(chess.makeMove(start, end) == MOVE_ERROR_NO_PIECE);

  //pawn b5 to a4 valid and takes piece                                                          
  start.x =  1; //case insenstive                                                                        
  start.y = 6;
  end.x = 0; //case insensitive                                                                          
  end.y = 3;
  assert(chess.makeMove(start, end) == MOVE_ERROR_NO_PIECE);

  //pawn e7 to e5 valid
  start.x =  4; //case insenstive                                                                        
  start.y = 6;
  end.x = 4; //case insensitive                                                                          
  end.y = 4;
  assert(chess.makeMove(start, end) == 123);

  //queen d8 to h4 valid                                                       
  start.x =  3; //case insenstive                                                                        
  start.y = 7;
  end.x = 7; //case insensitive                                                                          
  end.y = 3;
  assert(chess.makeMove(start, end) == 123);

  //queen h4 to f2 is valid and takes a pawn -- results in white being in check
  start.x =  7; //case insenstive                                                                        
  start.y = 3;
  end.x = 5; //case insensitive                                                                          
  end.y = 1;
  assert(chess.makeMove(start, end) == 123);

  chess.printBoard();
}

//test to see if Prompts stuff switch statement works                           
void test_checkPrompts() {
  ChessGame chess;
  assert(chess.checkPrompts(MOVE_CHECKMATE,chess.playerTurn()) == GAME_OVER);
  assert(chess.checkPrompts(MOVE_STALEMATE,chess.playerTurn()) == GAME_OVER);
  assert(chess.checkPrompts(MOVE_CHECK,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_CAPTURE,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_OUT_OF_BOUNDS,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_NO_PIECE,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_ILLEGAL,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_BLOCKED,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_MUST_HANDLE_CHECK,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_CANT_EXPOSE_CHECK,chess.playerTurn()) == 0);
  assert(chess.checkPrompts(MOVE_ERROR_CANT_CASTLE,chess.playerTurn()) == 0);
}

//checks to see if menuOption works--validity of user input                      
void test_menuOption() {
  ChessGame chess;  
  string input = "1";
  assert(chess.menuOption(input) == 1);
  input = "abc";
  assert(chess.menuOption(input) == 0);
  input = "12";
  assert(chess.menuOption(input) == 0);
  input = "fasdfgw";
  assert(chess.menuOption(input) == 0);
  input = "f";
  assert(chess.menuOption(input) == 0);
  input = "-1";
  assert(chess.menuOption(input) == 0);
}

//looks to see if the user input can be parsed/is generally of the correct form 
void test_runOption() {
  ChessGame chess;
  string input = "b2 b4";
  assert(chess.runOption(input) == 1);
  input = "a8 a7";
  assert(chess.runOption(input) == 1);
  input	= "a1 a2";
  assert(chess.runOption(input) == 1);
  input	= "h8 h1";
  assert(chess.runOption(input) == 1);
  input	= "e4 e5";
  assert(chess.runOption(input) == 1);
  input	= "afdsfa";
  assert(chess.runOption(input) == -7);
  input	= "1 2";
  assert(chess.runOption(input) == -7);
  input	= "e2";
  assert(chess.runOption(input) == -7);
}

//this checks whether the board is on or off                                  
//so if it gives 1 it goes to 0 and vice versa  
void test_checkBoard() {
  ChessGame chess;
  assert(chess.checkBoard(1) == 0);
  assert(chess.checkBoard(0) == 1);
}

int main() {

  test_makeMove();
  test_checkPrompts();
  test_menuOption();
  test_runOption();
  test_checkBoard();
  test_validMovePawn();
  test_validMoveRook();
  test_validMoveQueen();
  test_validMoveBishop();
  test_validMoveKing();
  test_validMoveKnight();

  cout << endl << "Success! All tests passed!" << endl;

    return 0;
}
