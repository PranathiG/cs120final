#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"


int main() {
    ChessGame chess;

    string input1;
    string fileName;
    int isValid = 0;
    while (!isValid) { //so repeats until a 1 or 2 is inputted
      Prompts::menu();
      getline(cin, input1);
      isValid = chess.menuOption(input1);
    }

    if (isValid != -10) {
        chess.run();
    }

    //so if load failed, isValid = 2 but it won't run and it'll get to here where we can free the memory                                                                                                    
    //MAKE SURE TO FREE ALL MEMORY!!!!                                                                 
}


