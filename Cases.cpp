#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <string.h>
#include <math.h> 
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;

int ChessGame::specialCircumstances(Position start, Position end) { //true >= 0, false < 0
  Piece* testPointer;
  testPointer = getPiece(start);

  Piece* testEnd;
  testEnd = getPiece(end);
  m_pieces[index(end)] = testPointer;
  m_pieces[index(start)] = nullptr;
  int anyCheck = isinCheck();
  m_pieces[index(start)] = testPointer;
  m_pieces[index(end)] = testEnd;
 
  return anyCheck; 
}

int ChessGame::isinCheck() {
  Piece* king; 
  Position end;
  Position start;
  Piece* testPiece;
  int retCode = 678;
  for (int i = 0; i < 64; i++) {
    if (m_pieces[i] != 0) {
      if (m_pieces[i]->id() == KING_ENUM) { //piece could be player1 or player2’s piece
	end.x = i%8; 
	end.y = i/8;
	king = getPiece(end);
	for (int i = 0; i < 64; i++) {
	  if (m_pieces[i] != 0) {
	    start.x = i%8;
	    start.y = i/8;
	    testPiece = getPiece(start);
	    if (testPiece != NULL) {
	      if (king->owner() == this->playerTurn()) { //so the king is player1’s king
		//check if player1 has put themselves in check
		//use king’s place as end for all player2’s pieces (start being their current position)
		return this->iAmInCheck(start, end);
	      } 
	      }
	    }
	  }
	  
	}
      }
  }
  return retCode;
}

int ChessGame::iAmInCheck(Position start, Position end) {
  Piece* testPiece;
  testPiece = getPiece(start);
  int retCode;
  if (testPiece->owner() != this->playerTurn()) {
    retCode = testPiece->validMove(start, end, *this);
    if (retCode >= 0) {
      if (this->playerTurn() == 0) {
	if (whiteWasChecked > 0) {
	  return MOVE_ERROR_MUST_HANDLE_CHECK;
	}
      } else {
	if (blackWasChecked > 0) {
	  return MOVE_ERROR_MUST_HANDLE_CHECK;
	}
      }
      return MOVE_ERROR_CANT_EXPOSE_CHECK; //true;validmovefor player2, return that player1 is in check & throw out error                                               
    }
  }
  return 678;
}

int ChessGame::otherPlayerInCheck(Position start, Position end) {
  Piece* king;
  Piece* testPiece;
  int retCode = 678;
  for (int i = 0; i < 64; i++) {
    if (m_pieces[i] != 0) {
      if (m_pieces[i]->id() == KING_ENUM) { //piece could be player1 or player2’s piece                                   
        end.x = i%8;
        end.y = i/8;
        king = getPiece(end);
        for (int i = 0; i < 64; i++) {
          if (m_pieces[i] != 0) {
            start.x = i%8;
            start.y = i/8;
            testPiece = getPiece(start);
            if (testPiece != NULL) {
              if (king->owner() != this->playerTurn()) {
		if (testPiece->owner() == this->playerTurn()) {
		  retCode = testPiece->validMove(start, end, *this);
		  if (retCode >= 0) {
		    if (this->playerTurn() == 0) {
		      blackWasChecked++;
		    } else {
		      whiteWasChecked++;
		    }
		    return MOVE_CHECK; //return that the player2 is in check                                                                                                          
		  }
		}
		}
		
	      }
	    }
	  }
	}
      }
    }return 678;
  }

int ChessGame::anyValidMoves() {
  Piece* testPiece;
  Position start;
  Position end;
  int retCode;
  Piece* testEnd;
  for (int i = 0; i < 64; i++) {
    if (m_pieces[i] != 0) {
      //m_pieces[i]->id();
      start.x = i%8;
      start.y = i/8;
      testPiece = getPiece(start);
      if (testPiece != NULL) {
	if (testPiece->owner() != this->playerTurn()) {
	  for (int j = 0; j < 64; j++) { // check every end piece
	    end.x = j%8;
	    end.y = j/8;
	    testEnd = getPiece(end);
	    retCode = testPiece->validMove(start, end, *this);	
	    
	    if (retCode >= 0) {
	      m_pieces[index(end)] = testPiece;
	      m_pieces[index(start)] = nullptr;
	      if (otherPlayerInCheck(start, end) == 678) {
		m_pieces[index(start)] = testPiece;
		m_pieces[index(end)] = testEnd;
		return 123;
	      }  
	      m_pieces[index(start)] = testPiece;
	      m_pieces[index(end)] = testEnd;
	    }
	  }
	}
      }
    }
  }
  return MOVE_CHECKMATE;
}
